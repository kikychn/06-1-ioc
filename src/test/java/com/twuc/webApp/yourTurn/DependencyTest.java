package com.twuc.webApp.yourTurn;

import com.twuc.webApp.OutOfScanningScope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;


public class DependencyTest {
    private AnnotationConfigApplicationContext context1;
    private AnnotationConfigApplicationContext context2;

    @BeforeEach
    void setUp() {
        context1 = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        context2 = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    // 2.1 - 1
    @Test
    void test_without_dependency_object() {
        WithoutDependency bean = context1.getBean(WithoutDependency.class);
        assertNotNull(bean);

        // extend
        WithoutDependency bean1 = context1.getBean(WithoutDependency.class);
        assertNotSame(bean, bean1);

        WithoutDependency bean2 = context2.getBean(WithoutDependency.class);
        assertNotSame(bean, bean2);
    }

    // 2.1 - 2
    @Test
    void test_with_dependency_object() {
        WithDependency bean = context1.getBean(WithDependency.class);
        assertNotNull(bean);
    }

    // 2.1 - 3
    @Test
    void should_throw_exception_test_OutOfScanningScope() {
        assertThrows(RuntimeException.class,()->{context1.getBean(OutOfScanningScope.class);});
    }

    // 2.1 - 4
    @Test
    void test_get_bean_from_interface_with_InterfaceImpl() {
        Interface bean = context1.getBean(Interface.class);

        assertNotNull(bean);
    }

    // 2.1 - 5
    @Test
    void should_return_simple_dependent_name_O_o() {
        SimpleInterface bean = context1.getBean(SimpleInterface.class);

        assertEquals("O_o", bean.simpleDependent().getName());
    }

    // 2.2 - 1
    @Test
    void should_use_constructor_with_dependent() {
        MultipleConstructor bean = context1.getBean(MultipleConstructor.class);

        assertEquals("This is the constructor with Dependent param", bean.getConstructorInfo());
    }

    // 2.2 - 2
//    @Test
//    void test_constructor_and_initialize_are_used_or_not() {
//        WithAutowiredMethod bean = context1.getBean(WithAutowiredMethod.class);
//
//        assertEquals("h",bean.getRunlog());
//    }
}
