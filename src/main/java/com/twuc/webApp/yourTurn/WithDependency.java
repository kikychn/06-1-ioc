package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private final Dependent dependent;

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }
}
