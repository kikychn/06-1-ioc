package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

//@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependent anotherDependent;
    private List<String> runlog;

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
        runlog.add("constructor is called. " + dependent.toString());
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.anotherDependent = anotherDependent;
        runlog.add("initialize is called. " + anotherDependent.toString());
    }

    public List<String> getRunlog() {
        return runlog;
    }
}
