package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObject implements SimpleInterface {

    @Bean
    public SimpleDependent simpleDependent() {
        SimpleDependent simpleDependent = new SimpleDependent();
        simpleDependent.setName("O_o");
        return simpleDependent;
    }
}
