package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private String message;
    private Dependent dependent;
    private String constructorInfo;

    public String getConstructorInfo() {
        return constructorInfo;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
        this.constructorInfo = "This is the constructor with Dependent param";
    }

    public MultipleConstructor(String message) {
        this.message = message;
        this.constructorInfo = "This is the constructor with String param";

    }

    public String getMessage() {
        return message;
    }

    public Dependent getDependent() {
        return dependent;
    }
}
